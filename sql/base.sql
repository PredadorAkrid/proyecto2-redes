create table cliente_usuario(
	id int primary key,
	username varchar(100) not null UNIQUE,
	pass text not null
);

create table archivos_cliente(
	id_archivo serial not null,
	nombre_archivo varchar(100),
	extension_archivo varchar(100),
	username_fk varchar(100) not null,
	archivo_binario BYTEA  not null,
    CONSTRAINT pk_archivos_cliente PRIMARY KEY (id_archivo, username_fk),
    CONSTRAINT fk_cliente	FOREIGN KEY (username_fk) REFERENCES cliente_usuario (username)
);

