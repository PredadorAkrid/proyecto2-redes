.PHONY: pipinstall dependencias cliente servidor
pipinstall:
	sudo apt install python3-pip
dependencias:
	sudo apt install libpq-dev python3-dev
	pip3 install -r requirements.txt
cliente:
	python3 python/cliente.py
servidor:
	python3 python/Servidor.py
