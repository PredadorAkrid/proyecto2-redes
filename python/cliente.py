# encoding UTF-8

import socket
import os
import re
#definimos el byte 0
#Aquí inicia el código del cliente
byte_cero=b"0"
#hacemos la conexión al servidor
conexion=('localhost', 9999)
s=socket.socket()
s.connect(conexion)
cadena = ""
bandera_salida = True
print(s.recv(60).decode())
#Pedimos el inicio de sesión o registro en cuyo caso
while bandera_salida:
	cadena=input()
	#Caso de inicio de sesión
	if(cadena == "10"):
		byte_registro = "10"
		#Mandamos la operación al servidor para que ejecute el inicio de sesión
		s.send(byte_registro.encode())
		#Validamos que la cadena no contenga nuestro delimitador
		salida = True
		while salida:
			usuario = input("Escribe nombre de usuario ")
			password= input("Escriba su clave secreta ")	
			if("&" not in usuario):
				if("&" not in password):
					salida = False
				else:
					salida = True
					print("Caracter & inválido")
			else:
				salida = True
				print("Caracter & inválido")
		
		#Preparamos el mensaje y lo codificamos
		mensaje="10"+"&"+usuario+"&"+password
		
		s.send(mensaje.encode())
		
		#Recibimos la respuesta del servidor y validamos
		error_inicio = s.recv(100)
		#print("Esto es error inicio: ", error_inicio)
		#Si el registro fue inválido, volvemos a pedir
		if(error_inicio.decode() == '99'):
			print("Credenciales inválidas")
			#print("Escriba operacion, 10 Inicio , 11 Registro")
			bandera_salida = True
			aux = s.recv(100)
			print(aux.decode())
		#Siel registro fue correcto salimos y preparamos el perfil del cliente
		else:
			print("Credenciales correctas")
			bandera_salida = False
			
	# El caso de registro de usuario
	elif(cadena == "11"):	
		byte_registro = "11"
		#Mandamos la operación para preparar al servidor para registar un usuario
		s.send(byte_registro.encode())
		print(s.recv(60).decode())
		#Pedimos los datos
		print("Usuario: ")
		usuario_registro = input()
		print("Contrasenia: ")
		pwsd_registro = input()
		#Validamos la longitud de cada campo
		if( (len(usuario_registro) > 64) or (len(pwsd_registro) > 16)):
			#En caso de error, avisamos al servidor
			envio="99"
			s.send(envio.encode())
			print(s.recv(300).decode())
			#Volvemos a pedir inicio de sesión o registro
			bandera_salida = True
		else:
			#Si los datos son correctos los enviamos al servidor para el registro
			datos_registro = byte_registro+"&"+usuario_registro+"&"+pwsd_registro
			s.send(datos_registro.encode())
			bandera_salida = False

	else:
		print("Parámetro inválido")
		bandera_salida = True
		cadena = ""


#Parte de mostrar perfil

while True:
	r = s.recv(100).decode()
	print(r)
	operacion_usuario = input("Ingrese la opción [20|30|101]: ")

	if(operacion_usuario == "20"):
		s.send(operacion_usuario.encode()) 
		mensaje_archivo = s.recv(100)
		print(mensaje_archivo.decode())
		nombre_archivo = input("Ruta archivo: ")
		
		s.send(nombre_archivo.encode())
		extension = input("Escriba la extension del archivo: ")
		s.send(extension.encode())
		print(s.recv(100).decode())

		validado = s.recv(18).decode()
		
		if(validado == "99"):
			print("Error al cargar el archivo")
		else:
			print(validado)
			res = s.recv(28)
			print(res.decode())
			if(res.decode() != "99"):
				pass
			
	#caso de visualizar archivo
	elif(operacion_usuario == "30"):
		s.send(operacion_usuario.encode())
		#recibimos la cadena donde indicamos los archvios al usuario
		archivos = s.recv(1024)
		#validamos si es un error
		if((archivos.decode() == "Error al cargar archivos") or (archivos.decode() == "No se encontró el archivo")):
			print("No se pudieron cargar archivos o no hay archivos para mostrar")
		else:
			#oec procedemos a pedirle al usuario el id del archivo que desea solicitar
			print(archivos.decode())
			id_archivo = input("Id archivo: ")
			#enviamos el id del archivo
			s.send(id_archivo.encode())
			#recibimos el nombre del archivo
			long_nombre = s.recv(15+3+3+1).decode()
			datos = long_nombre.split("&")
			respuesta_a = s.recv(int(datos[1]))

			#verificamos si el archivo existia
			if(datos[0] != "99"):
				
				
				#escribimos el archivo
				binary_file = open(datos[0], "wb") 
				binary_file.write(respuesta_a)
				binary_file.close()
				print("El archivo se escribio correctamente")
			else:
				print("El archivo indicado no existe")
	#Código para cerrar sesión
	elif(operacion_usuario == "101"):
		print("Petición para cerrar sesión desde usuario")
		salida = "101"
		s.send(salida.encode())
		break
	#EOC error
	else:
		s.send(operacion_usuario.encode())
		print(s.recv(100).decode())
		

