# encoding UTF-8
import psycopg2
from psycopg2 import Error
import clase_cliente as cli
from Cliente_2 import Cliente
from pathlib import Path
import os
import threading
from threading import Thread
import socket
import ssl
import time
import re

#Extendemos a Thread
class Proyecto(Thread):
    """Clase principal Proyecto encargada de inicializar el servidor del programa
    """
    
    def __init__(self, conn, addr):
        """Constructor de la clase Proyecto
        Args:
            conn (str): una conexión con un socket al servidor
            addr (str): una dirección ip desde la cual se están conectando.
        """

        # Inicializar clase padre.
        Thread.__init__(self)
        
        self.conn = conn
        self.addr = addr
    
    def run(self):
        """Método para gestionar las peticiones del cliente en el servidor
        """
        operacion = b"1"
        codigo_error = b"99"
        while True:
            while operacion !=b"101":
                mensaje="Escriba operacion, 10 Inicio , 11 Registro"
                self.conn.send(mensaje.encode())
                respuesta_cliente=self.conn.recv(2)
                print(respuesta_cliente)
                if(respuesta_cliente == b'10'): ###########################################################################################
                        datos_inicio_sesion=self.conn.recv(84)
                        datos = datos_inicio_sesion.decode() 
                        partes=datos.split("&")
                        codigo= partes[0]
                        nombre=partes[1]
                        password=partes[2]                      
                        dumy=Cliente("","")
                        if(dumy.buscaBaseDeDatos(nombre,password)):   
                            inicio_correcto = "Inicio exitoso"
                            
                            self.conn.send(inicio_correcto.encode())
                            salida = self.muestraInicio(self.conn, nombre)
                            if(salida):
                                print("inicio sesion")
                            
                                operacion = b"101"
                            else:
                                while(salida != True):
                                    salida = self.muestraInicio(self.conn, nombre)
                                operacion = b"101"                         
                        else:
                            print("Error en las credenciales")
                            mensaje_credenciales="99"
                            self.conn.send(mensaje_credenciales.encode())
                            operacion = b"1"   
                elif(respuesta_cliente == b'11'): ###########################################################################################
                    mensaje_registro = "Introduzca su usuario y contraseña de registro."
                    self.conn.send(mensaje_registro.encode())
                    respuesta_cliente=self.conn.recv(2)
                    respuesta_cliente_op = respuesta_cliente.decode()
                    if(respuesta_cliente_op == "99"):
                        print("entra al error")
                        error_registro = "Ocurrio un error en el registro, el usuario solo puede tener una\n" + " una longitud de 64 y la contraseña de 16."
                        self.conn.send(error_registro.encode())
                    else:
                        print("entra al else")
                        respuesta_cliente=self.conn.recv(82).decode()
                        argumentos_registro = respuesta_cliente.split('&')
                        print(argumentos_registro[1],argumentos_registro[2])
                        cliente_nuevo = cli.RegistroCliente(argumentos_registro[1],argumentos_registro[2])
                        salida = self.muestraInicio(self.conn, argumentos_registro[1])
                        if(salida):
                            
                            operacion = b"101"
                        else:
                            while(salida != True):
                                    salida = self.muestraInicio(self.conn, nombre)
                            operacion = b"101"    
                else:
                    mensaje_error = "Código de error 99. Escriba una opcion valida."
                    self.conn.send(mensaje_error.encode())
                break
            if(operacion == b"101"):
                
                self.conn.close()
                break
            else:
                print("aqui debe acabar")

    def muestraInicio(self,socket, usuario):
        """Constructor de la clase Proyecto
        Args:
            socket (Conexion): La conexion con el cliente
            usuario (string): El nombre del usuario que ejecutó la conexión
        Returns:
            Bool (boolean):  Un booleano si se decide cerrar la conexión.
        """
        while True:
            mensajeInicio = "Escriba 20 para cargar archivo, 30 para visualizar archivo, 101 para cerrar sesion"
            socket.send(mensajeInicio.encode())
            solicitud_archivo = socket.recv(3)
            solicitud_archivo_op = solicitud_archivo.decode()
            print(solicitud_archivo_op)
            if(solicitud_archivo_op == "20"):
                nombre_archivo = "Escribe la ruta del archivo"
                socket.send(nombre_archivo.encode())
                nombre_archivo = socket.recv(100).decode()
                nombre_fin = nombre_archivo.split("/")
                nombre_fin = nombre_fin[len(nombre_fin)-1]
                print("el nombre del archivo es: ", nombre_fin)
                extension = socket.recv(100).decode()
                
                temp_cliente = Cliente("","")
                try:
                    archivo_encontrado = "El archivo fue encontrado, cargando"
                    socket.send(archivo_encontrado.encode())
                    f = open(nombre_archivo,'rb')
                    filedata = f.read()

                    
                    if(temp_cliente.cargaArchivo(nombre_fin, extension, usuario , filedata)):

                        exito = "se cargo con exito"
                        socket.send(exito.encode())
                        socket.send("finalizando carga de archivo".encode())

                        
                    else:
                        error_carga = "99"
                        socket.send(error_carga.encode())
                except(Exception) as error:
                    print(error)
                    archivo_invalido = "99"
                    socket.send(archivo_invalido.encode())
            elif(solicitud_archivo_op == "30"):
                temp_cliente = Cliente("","")
                try:
                    data = temp_cliente.visualizarArchivos(usuario)
                    if(data == None):
                        error_base = "Error al cargar archivos"
                        socket.send(error_base.encode())
                    elif(len(data) == 0):
                        no_archivos = "No se encontró el archivo"
                        socket.send(no_archivos.encode())
                    else:
                        aux_data = []
                        
                        archivo_data = "Introduzca el id del archivo que desea consultar\n"
                        archivo_data += "Id   " + "Nombre archivo  \n"
                        for elem in data:
                            aux_data.append(elem)
                            archivo_data += "\n   "+ str(elem[0]) + "     "  +  str(elem[1])
                        
                        socket.send(archivo_data.encode())
                        filtro = socket.recv(100)
                        encontrado = False
                        for elem in data:
                            if(int(elem[0]) == int(filtro.decode())):
                                busqueda = elem
                                encontrado = True
                                
                        if(encontrado):


                            
                            

                            longitud = busqueda[4].tobytes()
                            aux = busqueda[1] + "&" + str(len(longitud.decode())) + "&"
                            while(len(aux) < 22):
                            	aux += "0"

                            socket.send(aux.encode())
                            socket.send(busqueda[4])


                        else:
                            socket.send("99".encode())
                            socket.send("".encode())
                            socket.send("".encode())
                            socket.send("".encode())

                                
                except(Exception) as error:
                    print(error)
                    error_visualizacion = "Error al cargar archivos"
                    socket.send(error_visualizacion.encode())


                
            elif(solicitud_archivo_op == "101"):
                return True
            else:
                error_codigo = "Introduce un codigo correcto"
                socket.send(error_codigo.encode())
                pass


def main():
    """Método principal de la clase Proyecto
    """    
    conexion=("",10001)
    s=socket.socket()
    #establecemos un timeout
    s.settimeout(1000)
    s.bind(conexion) 
    s.listen()
    operacion = b'1'
    codigo_error = b'99'
    while True :
        s_conexion,dir_direccion=s.accept()
        c = Proyecto(s_conexion, dir_direccion)
        c.start() 
# función mágina para inicializar el método main       
if __name__ == "__main__":
    main()
