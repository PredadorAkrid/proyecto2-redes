import psycopg2
import hashlib, os, binascii

class Cliente():   
    """Clase Cliente que sirve para ejecutar operaciones en la base de datos por parte del cliente
    """
    id=""
    usuario=""
    password=""
    archivos=""

    def __init__(self,usuario,password):
        """Constructor de la clase Cliente
        Args:
            usuario (str): el nombre del usuario a registrar `usuario`.
            password (str): la contraseña del usuario para guardarla`password`.
        """
        self.usuario=usuario
        self.password=password
            
    def getConexionBase(self):
        """Método para realizar la conexión a la base de datos através de psycopg2

        Returns:
            Conexion: La conexión hacia la base de datos 
        """

        conexion = psycopg2.connect(host="localhost", database="usuarios_redes", user="postgres", password="1234")
        return conexion
    
    def buscaBaseDeDatos(self,nombre,password):
        """Método para buscar si un usuario está registrado en la base de datos
        Args:

        Returns:
            Conexion: La conexión hacia la base de datos 
        """
        conexion = self.getConexionBase()
        cursor= conexion.cursor()
        dummy=Cliente("","")
        cursor= conexion.cursor()
        datos= "select username,pass from cliente_usuario;"   
        cursor.execute(datos)
        lista=cursor.fetchall()
        for i in lista:
            dummy.setUsuario(i[0])
            dummy.setPassword(i[1])
            #verificamos que la contraseña coincida con las de la base de datos
            if(dummy.getUsuario()==nombre and self.verify_password(dummy.getPassword(),password)):
                return True                
        return False
    """    
    def enviaDatos(self, nombre,password):
        
        if(self.buscaBaseDeDatos(nombre,password)):
            cadena= "11&" + nombre + '&' + password 
            return cadena
        else:
            return "99&Noencontrado"
    """

    def cargaArchivo(self, nombre, extension, usuario , archivo):
        """Método para cargar un archivo en la base de datos
        Args:
            nombre (string): el nombre del archivo.
            extension (string): la extensión del archivo.
            usuario (string): el usuario que inicia la petición.
            archivo (ByteArray): el arreglo de bytes del archivo.
        Returns:
            Bool (boolean): True si guarda correctamente en la base, False en otro caso.
        """
        try:

            conexion = self.getConexionBase()
            cursor= conexion.cursor()
            sql = "INSERT INTO archivos_cliente (nombre_archivo,extension_archivo, username_fk, archivo_binario) VALUES (%s,%s, %s, %s)"
            params = (nombre, extension, usuario, archivo)    
            cursor.execute(sql,params) 
            conexion.commit()
            return True
        except (Exception, psycopg2.DatabaseError) as error:
            print("Hubo un error al cargar el archivo", error)
            return False
        
    def visualizarArchivos(self, usuario):
        """Método para traer de la base de datos todos los archivos del usuario
        Args:
            usuario (string): el usuario que inicia la petición.
        Returns:
            Object (Object): Regresa la lista con los registros si el query no retorna una lista vacía, [] eoc, None si ocurre un error
        """
        try:

            conexion = self.getConexionBase()
            cursor= conexion.cursor()
            cursor.execute("SELECT * FROM archivos_cliente WHERE username_fk = %(username)s", {'username': usuario});
            rows = []
            rows = cursor.fetchall()
            if(rows != []):
                return rows
            else:
                return []
        except (Exception, psycopg2.DatabaseError) as error:
            print("Hubo un error al cargar los archivos del usuario", error)
            return None
    

    ##Metodos get y set usuario
    
    def getUsuario(self):
        """Método getter para obtener al usuario
        Returns:
            usuario (string): el nombre del usuario
        """
        return self.usuario
    
    def setUsuario(self,usuario):
        """Método setter para sobreescribir el nombre del usuario
        Args:
            usuario (string): el nuevo nombre del usuario
        """
        self.usuario=usuario
        
    ## Metodo get y set password
    def getPassword(self):
        """Método getter para obtener la contraseña del usuario
        Returns:
            password (string): la clave del usuario
        """
        return self.password
    def setPassword(self,password):
        """Método setter para sobreescribir la clave del usuario
        Args:
            usuario (string): el nuevo nombre del usuario
        """
        self.password=password

    def verify_password(self,stored_password, provided_password):
        """Método para verificar la contraseña recibida con la almacenada en la base de datos
        Args:
            stored_password (string): La contraseña guardada en la base de datos
            provided_password (string): La contraseña del usuarios_redes
            Returns:
            Bool (boolean): True si las contraseñas son consistentes, False en otro caso
        """
        salt = stored_password[:64]
        stored_password = stored_password[64:]
        pwdhash = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'),salt.encode('ascii'),100000)
        pwdhash = binascii.hexlify(pwdhash).decode('ascii')
        return pwdhash == stored_password
    
 
