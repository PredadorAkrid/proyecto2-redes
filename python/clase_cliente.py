# encoding UTF-8

import psycopg2
from psycopg2 import Error
import random
import hashlib, os, binascii

class RegistroCliente():

    """Clase RegistroCliente para cargar nuevos usuarios en la base de datos
    """

    #variables de clase
    id_cliente  = None
    usuario = ""
    password = ""
    claveCifrada  = ""

    def __init__(self,usuario,password):
        
        """Constructor de la clase RegistroCliente
        Args:
            usuario (str): el nombre del usuario a registrar `usuario`.
            password (str): la contraseña del usuario para guardarla`password`.
        """

        self.usuario=usuario
        self.password=password   
        cursor = None
        #Intentamos registrar al cliente
        try:
            print("entra al cliente")
            conexion = self.getConexion()
            cursor = conexion.cursor()
            claveCifrada = self.hash_password(password)
            random_number = random.randint(0,1000)
            cursor.execute("SELECT * FROM cliente_usuario WHERE id = %(id)s", {'id': random_number});
            rows = cursor.fetchall()
            if(len(rows) != 0):
                while(len(rows)!=0):
                    random_number = random.randint(0,1000)
                    cursor.execute("SELECT * FROM cliente_usuario WHERE id = %(id)s", {'id': random_number});
                    rows = cursor.fetchall()
                sql = "INSERT INTO cliente_usuario (id,username, pass) VALUES (%s,%s, %s)"
                val = (random_number, usuario, claveCifrada)
                print("caso1")
                
                cursor.execute(sql,val) 
                conexion.commit()       
            else:
                print("caso2")
                sql = "INSERT INTO cliente_usuario (id,username, pass) VALUES (%s,%s, %s)"
                val = (random_number, usuario, claveCifrada)
                cursor.execute(sql,val)            
                conexion.commit()
        #Si algo sale mal imprimirá el error al usuario
        except (Exception, psycopg2.DatabaseError) as error:
            print("Hubo un error al crear al usuario: ", error)
        #Cerramos la conexión a la base de datos
        finally:
            if (cursor):
                #cursor.close()
                cursor.close()
            else:
                print("Caso no contemplado")
   

    def getConexion(self):
        """función para realizar la conexión a la base de datos através de psycopg2

        Returns:
            Conexion: La conexión hacia la base de datos 
        """
        try:
            connection = psycopg2.connect(
                                      user="postgres",
                                      password="1234",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="usuarios_redes")
            return connection
        except (Exception, Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return None
    
    def hash_password(self,password):
        
        """función para realizar la conexión a la base de datos através de psycopg2
        Args:
            password (string): La contraseña sin cifrar del usuario
        Returns:
            HashedPassword: La contraseña cifrada del usuario
        """

        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        return (salt + pwdhash).decode('ascii')

